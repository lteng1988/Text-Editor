#include "notepad.h"
//#include "ui_notepad.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QtWidgets>

#include <QtNetwork>
#include <QWebView>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QSettings>

#include <QProcess>

Notepad::Notepad()
{
    main = centralWidget();

    initializeVariables();

    textEdit = new QPlainTextEdit;
//    setCentralWidget(textEdit);
    textEdit->setStyleSheet("background-color: black; color:white;");
    stackedWidget = new QStackedWidget;
    stackedWidget->addWidget(textEdit);

//    setWindowTitle("Phantom Editor");

    webView = new QWebView;
    connect(webView, SIGNAL(urlChanged(QUrl)), this, SLOT(authPageUrlChanged()));

    connect(webView, SIGNAL(titleChanged(QString)), this, SLOT(authPageTitleChanged()));

    stackedWidget->addWidget(webView);

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    readSettings();

    connect(textEdit->document(), SIGNAL(contentsChanged()),
            this, SLOT(documentWasModified()));
    setCurrentFile("");
    setUnifiedTitleAndToolBarOnMac(true);

    readTokens();
    setCentralWidget(stackedWidget);
    findOutlook();

//    main->deleteLater();

//    getAuthCodeForDrive();
//    saveUserCredentialsForGoogleDrive();

}



Notepad::~Notepad()
{
    //delete ui;
}

/**
 * @brief Notepad::on_pushButton_clicked
 * Define button function
 */
//void Notepad::on_pushButton_clicked()
//{
//    qApp->quit();
//}

//void Notepad::on_actionOpen_triggered()
//{
//    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QString(), tr("Text Files (*.txt);; C++ Files (*.cpp *.h)"));
//    Notepad::currentFileName = fileName;

//    if (!fileName.isEmpty()){
//        QFile file(fileName);
//        if (!file.open(QIODevice::ReadOnly)){
//            QMessageBox::critical(this, tr("Error"), tr("Cloud not open file. Read Only"));
//            return;
//        }

//        QTextStream in(&file);
//        ui->textEdit->setText(in.readAll());
//        file.close(); //Close file
//    }
//}

//void Notepad::write_to_file(QString fileName)
//{
//    if (!fileName.isEmpty()){
//        QFile file(fileName);
//        if (!file.open(QIODevice::WriteOnly)){
//            // Error message
//        } else {
//            QTextStream stream(&file);
//            stream << ui->textEdit->toPlainText();
//            stream.flush();
//            file.close();
//        }
//    }

//}

//void Notepad::on_actionSave_triggered()
//{
//    QString fileName = Notepad::currentFileName;

//    Notepad::write_to_file(fileName);
//}

//void Notepad::on_actionSave_As_triggered()
//{
//    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QString(), tr("Text Files (*.txt);; C++ Files(*.cpp *.h)"));

//    Notepad::write_to_file(fileName);
//}

void Notepad::initializeVariables()
{

//    outlookCmd = QString("/m \" body=" + textEdit->toPlainText() + "\"");
    redirectUri = QString("urn:ietf:wg:oauth:2.0:oob:auto");
    clientId = QString("1081456849926-k0o1ldpnjtmsh9ur38uq3957iugo8fem.apps.googleusercontent.com");
    clientSecret = QString("jQ2bo165z-RggrhePAuqgDvN");

}

void Notepad::newFile()
{
    if (maybeSave()){ //If file was modify
        textEdit->clear();
        setCurrentFile("");
    }
}

void Notepad::open(){
    if (maybeSave()){
        QString fileName = QFileDialog::getOpenFileName(this);
        if (!fileName.isEmpty()){
            loadFile(fileName);
        }
    }

}

bool Notepad::save(){
    if (curFile.isEmpty()){
        return saveAs();
    } else {
        return saveFile(curFile);
    }
}

bool Notepad::saveAs(){
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList files;
    if (dialog.exec()){
        files = dialog.selectedFiles();
    }else{
        return false;
    }
    return saveFile(files.at(0));

}

void Notepad::documentWasModified(){
    setWindowModified(textEdit->document()->isModified());
}

void Notepad::createActions(){
    newAct = new QAction(QIcon(":/images/new.png"), tr("&new"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

    openAct = new QAction(QIcon(":/images/open.png"), tr("&open"), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open a file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save file"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(tr("&save"), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save as new file"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    exitAct = new QAction(tr("&exit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit my awesome notepad"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    cutAct = new QAction(QIcon(":/images/cut.png"), tr("&cut"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr("Cut selected text to clipboard"));
    connect(cutAct, SIGNAL(triggered()), this, SLOT(cut()));

    copyAct = new QAction(QIcon(":/images/copy.png"), tr("&copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr("Copy selected text to clipboard"));
    connect(copyAct, SIGNAL(triggered()), this, SLOT(copy()));

    pasteAct = new QAction(QIcon(":/images/paste.png"),tr("&paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr("Paste text from clipboard"));
    connect(pasteAct, SIGNAL(triggered()), this, SLOT(paste()));

    cutAct->setEnabled(false);
    copyAct->setEnabled(false);
    connect(textEdit, SIGNAL(copyAvailable(bool)),
            cutAct, SLOT(setEnabled(bool)));
    connect(textEdit, SIGNAL(copyAvailable(bool)),
            copyAct, SLOT(setEnabled(bool)));

    saveCredAct = new QAction(tr("&Google Drive Credentials"), this);
    saveCredAct->setStatusTip(tr("Enter Google Drive Credentials"));
    connect(saveCredAct, SIGNAL(triggered()), this, SLOT(getAuthCodeForDrive()));

    uploadCurrentFileAct = new QAction(tr("&Upload current file"), this);
    uploadCurrentFileAct->setStatusTip(tr("Upload current file to Google Drive"));
    connect(uploadCurrentFileAct, SIGNAL(triggered()), this, SLOT(uploadFile()));

    mailInOutlookAsBodyAct = new QAction(tr("&Email current text as body in Outlook"), this);
    mailInOutlookAsBodyAct->setStatusTip(tr("Email current text as body in Outlook"));
    connect(mailInOutlookAsBodyAct, SIGNAL(triggered()), this, SLOT(uploadFile()));

    mailInOutlookAsAttachmentAct = new QAction(tr("&Email current text as attachment in Outlook"), this);
    mailInOutlookAsAttachmentAct->setStatusTip(tr("Email current text as attachment in Outlook"));
    connect(mailInOutlookAsAttachmentAct, SIGNAL(triggered()), this, SLOT(uploadFile()));

}

void Notepad::createMenus(){
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);

    menuBar()->addSeparator();

    saveCredMenu = menuBar()->addMenu(tr("&Upload"));
    saveCredMenu->addAction(saveCredAct);
    saveCredMenu->addAction(uploadCurrentFileAct);

    emailMenu = menuBar()->addMenu(tr("&Email"));
    emailMenu->addAction(mailInOutlookAsBodyAct);
    emailMenu->addAction(mailInOutlookAsAttachmentAct);


//    helpMenu = menuBar()->addMenu(tr("*&hHelp"));
//    helpMenu->addAction()
}

void Notepad::createToolBars(){
    fileToolBar = addToolBar(tr("File"));
    fileToolBar->addAction(newAct);
    fileToolBar->addAction(openAct);
    fileToolBar->addAction(saveAct);

    editToolBar = addToolBar(tr("Edit"));
    editToolBar->addAction(cutAct);
    editToolBar->addAction(copyAct);
    editToolBar->addAction(pasteAct);


}

void Notepad::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));

}

void Notepad::readSettings(){
    QSettings settings("QtProject", "Application");
    QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
    QSize size = settings.value("size", QSize(400, 400)).toSize();
    resize(size);
    move(pos);
}

void Notepad::writeSettings(){
    QSettings settings("QtProject", "Application");
    settings.setValue("pos", pos());
    settings.setValue("size", size());
}

bool Notepad::maybeSave(){
    if (textEdit->document()->isModified()){
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                                   tr("The document has been modified. \n"
                                      "Do you want to save your changes?"),
                                   QMessageBox::Save | QMessageBox::Discard |
                                   QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return save();
        else if (ret == QMessageBox::Cancel)
            return false;

    }
    return true;
}

void Notepad::loadFile(const QString &fileName){
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)){
    QMessageBox::warning(this, tr("Application"),
                         tr("Cannot read file %1:\n%2")
                         .arg(fileName)
                         .arg(file.errorString()));
        return;
    }

    QTextStream in(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    textEdit->setPlainText(in.readAll());
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("File Loaded"));

}

bool Notepad::saveFile(const QString &fileName){
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)){
        QMessageBox::warning(this, tr("Application"),
                         tr("Cannot write to file %1:\n%2.")
                            .arg(fileName)
                            .arg(file.errorString()));
        return false;

    }

    QTextStream out(&file);

#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    out << textEdit->toPlainText();
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("File Save"));
    return true;
}
void Notepad::setCurrentFile(const QString &fileName){
    curFile = fileName;
    textEdit->document()->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if (curFile.isEmpty())
        shownName = "untitiled.txt";
    setWindowFilePath(shownName);

}

QString Notepad::strippedName(const QString &fullFileName){
    return QFileInfo(fullFileName).fileName();
}

void Notepad::authPageUrlChanged(){
    QUrlQuery temp(webView->url().toString());
    qDebug() << temp.queryItemValue("code");

    QString code = temp.queryItemValue("code");
    if (!code.isEmpty()){
        authCode = code;
        getAuthToken();
    }
}
void Notepad::authPageTitleChanged(){
    QString title = webView->title();
    if (title.contains(QString("code"))){
        QUrlQuery temp(title);
        QString code = temp.queryItemValue("code");
        authCode = code;
        getAuthToken();
    }
}

void Notepad::getAuthTokenWithRefreshToken(){
    qDebug() << "Getting auth tokens with refresh tokens";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("https://www.googleapis.com/oauth2/v3/token");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QJsonObject authTokensObj = authTokens.object();
    QString refreshToken = authTokensObj.value(QString("refresh_token")).toString();

    if (refreshToken.isEmpty()){

    }

    QUrlQuery params;
    params.addQueryItem("client_id", clientId);
    params.addQueryItem("client_secret", clientSecret);
    params.addQueryItem("refresh_token", refreshToken);
    params.addQueryItem("grant_type", "refresh_token");
    // etc

    QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(processAuthToken(QNetworkReply *)));

    manager->post(request, params.toString(QUrl::FullyEncoded).toUtf8());
}

void Notepad::getAuthToken(){
    QString code = authCode;
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("https://www.googleapis.com/oauth2/v3/token");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QUrlQuery params;
    params.addQueryItem("code", code);
    params.addQueryItem("client_id", clientId);
    params.addQueryItem("client_secret", clientSecret);
    params.addQueryItem("redirect_uri", redirectUri);
    params.addQueryItem("grant_type", "authorization_code");
    // etc

    QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(processAuthToken(QNetworkReply *)));

    manager->post(request, params.toString(QUrl::FullyEncoded).toUtf8());
    stackedWidget->setCurrentIndex(0);
}

bool Notepad::getAuthCodeForDrive(){
//    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("https://accounts.google.com/o/oauth2/auth");
//    QNetworkRequest request(url);

//    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QUrlQuery params;
    params.addQueryItem("scope", "https://www.googleapis.com/auth/drive");
    params.addQueryItem("state", "security_token%3D138r5719ru3e1%26url%3Dhttps://oa2cb.example.com/myHome");
    params.addQueryItem("redirect_uri", redirectUri);
    params.addQueryItem("response_type", "code");
    params.addQueryItem("client_id", clientId);
    params.addQueryItem("approval_prompt", "force");
    params.addQueryItem("include_granted_scopes", "true");

//    // etc

//    QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(processAuthCode(QNetworkReply *)));

//    manager->post(request, params.toString(QUrl::FullyEncoded).toUtf8());

    url.setQuery(params.query());

    webView->load(url);

//    setCentralWidget(webView);
    stackedWidget->setCurrentIndex(1);
    return true;
}

/**
 * @brief Notepad::processAuthCode
 * @param reply
 */
void Notepad::processAuthCode(QNetworkReply *reply){
    QString answer = QString::fromUtf8(reply->readAll());
    qDebug() << "End getting auth code";
    qDebug() << answer;
}

/**
 * @brief Notepad::processAuthToken
 * @param reply
 */
void Notepad::processAuthToken (QNetworkReply *reply){
    QString response = QString::fromUtf8(reply->readAll());
    qDebug() << "Done auth token";
    authTokens = QJsonDocument::fromJson(response.toUtf8());

    QJsonObject authTokensObj = authTokens.object();
    refreshToken = authTokensObj.value(QString("refresh_token")).toString();
    accessToken = authTokensObj.value(QString("access_token")).toString();
    tokenType = authTokensObj.value(QString("token_type")).toString();
    QSettings settings("QtProject", "Application");

    settings.setValue("refreshToken", refreshToken);
    settings.setValue("accessToken", accessToken);
    settings.setValue("tokenType", tokenType);

    qDebug() << authTokens;
    webView->load(QString(""));
}

void Notepad::readTokens()
{
    QSettings settings("QtProject", "Application");
    QString refreshToken = settings.value("refreshToken").toString();
    QString accessToken = settings.value("accessToken").toString();
    QString tokenType = settings.value("tokenType").toString();

    qDebug() << "loaded access token: " << accessToken;
    qDebug() << "loaded refresh token: " << refreshToken;
    qDebug() << "loaded token type: " << tokenType;

    if (!refreshToken.isEmpty() && !accessToken.isEmpty() && !tokenType.isEmpty()){
        disableDriveCredAct = true;
    }
}

bool Notepad::uploadFile(){
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("https://www.googleapis.com/upload/drive/v2/files?uploadType=media");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


    if (!accessToken.isEmpty() && !refreshToken.isEmpty() && !tokenType.isEmpty()){

//        QJsonObject authTokensObj = authTokens.object();
//        QString accessToken = authTokensObj.value(QString("access_token")).toString();
//        QString tokenType= authTokensObj.value(QString("token_type")).toString();

        request.setRawHeader("Authorization", QString(tokenType + " " + accessToken).toUtf8());
        request.setRawHeader("Content-Type", "text/plain");
        QString body = textEdit->toPlainText();
        QUrlQuery params;
        params.addQueryItem("uploadType", "media");
        QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(uploadFileFinihed(QNetworkReply *)));

        manager->post(request, body.toUtf8());
    }else{
        //Try refesh token
        getAuthTokenWithRefreshToken();

    }



    // etc



    return true;
}

void Notepad::uploadFileFinihed(QNetworkReply *reply){
    QString answer = QString::fromUtf8(reply->readAll());
    qDebug() << answer;
}

bool Notepad::getFiles(){
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("https://www.googleapis.com/upload/drive/v2/files?uploadType=media");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    QJsonObject authTokensObj = authTokens.object();
    QString accessToken = authTokensObj.value(QString("access_token")).toString();
    QString tokenType= authTokensObj.value(QString("token_type")).toString();

    request.setRawHeader("Authorization", QString(tokenType + " " + accessToken).toUtf8());
    request.setRawHeader("Content-Type", "text/plain");
    QString body = textEdit->toPlainText();



    QUrlQuery params;
    params.addQueryItem("uploadType", "media");
    // etc

    QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));

    manager->post(request, body.toUtf8());

    return true;
}

void Notepad::replyFinished(QNetworkReply *reply){
    QString answer = QString::fromUtf8(reply->readAll());
    qDebug() << answer;
}


bool Notepad::saveUserCredentialsForGoogleDrive(){
    QDialog *dlgMultiLine = new QDialog(this);
    QGridLayout *gridLayout = new QGridLayout(dlgMultiLine);
    QPlainTextEdit *txtMultiline = new QPlainTextEdit;

    txtMultiline->setObjectName(QString::fromUtf8("txtMultiline"));
    QLabel *login = new QLabel(tr("Login"));

    gridLayout->addWidget(login, 0, 0, 1, 1);

    QLineEdit *loginInput = new QLineEdit();

    gridLayout->addWidget(loginInput, 0, 1, 1, 1);

    QLabel *password = new QLabel(tr("Password"));

    gridLayout->addWidget(password, 1, 0, 1, 1);

    QLineEdit *passwordInput = new QLineEdit();
    passwordInput->setEchoMode(QLineEdit::Password);

    gridLayout->addWidget(passwordInput, 1, 1, 1, 1);
//    gridLayout->addWidget(txtMultiline, 0, 0, 1, 1);


    QDialogButtonBox *buttonBox = new QDialogButtonBox();
    buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
    buttonBox->setOrientation(Qt::Horizontal);
    buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

    gridLayout->addWidget(buttonBox, 2, 0, 1, 1);

    connect(buttonBox,SIGNAL(accepted()),dlgMultiLine,SLOT(accept()));
    connect(buttonBox,SIGNAL(rejected()),dlgMultiLine, SLOT(reject()));

    if (dlgMultiLine->exec() == QDialog::Accepted) {
        // If the user didn't dismiss the dialog, do something with the fields
        qDebug() << loginInput->text();
        qDebug() << passwordInput->text();
    }else if (dlgMultiLine->exec() == QDialog::Rejected){


    }

//    bool ok;
//    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
//                                         tr("Login: "), QLineEdit::Normal,
//                                         "", &ok);
//    if (ok && !text.isEmpty()){
//        QString a;
//        a = textEdit->toPlainText() + text;
//        textEdit->setPlainText(a);
//    }
    return true;
}

//@TODO: fix this
void Notepad::findOutlook(){

//    QString searchCmd = QString("for /F \"tokens=2*\" %a in ('REG QUERY \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\OUTLOOK.EXE\" /v \"Path\"') do echo %b");
//    qDebug() << searchCmd;

    QString searchCmd = QString("REG QUERY \"HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\OUTLOOK.EXE\" /v \"Path\"");
    process = new QProcess(this);
    process->start(searchCmd);

    connect(process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(searchOutlookFinished(int, QProcess::ExitStatus)));
    connect(process, SIGNAL(error(QProcess::ProcessError)), this, SLOT(searchOutlookError(QProcess::ProcessError)));

}


void Notepad::searchOutlookFinished(int code, QProcess::ExitStatus status){
    QString result;
    QStringList tokens;
    qDebug() << code;
    qDebug() << status;
//    qDebug() << process->readAll();
    do{
        result = process->readLine();
        if (result.indexOf(QString("REG_SZ")) != -1){
            qDebug() << "FOUND";
            tokens = result.split(('\t'));
            qDebug() << result;
            if (result.indexOf(":\\") != -1){
                if (result.indexOf(":\\") - 1 < 0){
                    break;
                }
                outlookPath = result.mid(result.indexOf(":\\") - 1, -1).trimmed();
            }

        }
//        qDebug() << result;
    }while (!result.isEmpty());
    qDebug() << "result: " << outlookPath;
}

void Notepad::searchOutlookError(QProcess::ProcessError error){
    qDebug() << "error";
    qDebug() << error;

}

bool Notepad::sendAsAttachmentInOutlook(){
    return true;
}


void Notepad::emailTextAsBody(){
    QString body = textEdit->toPlainText();
    QString outlookExe = "\"" + outlookPath + "OUTLOOK.EXE\"";
    QString cmd = outlookExe + outlookCmd;

    QProcess *process = new QProcess(this);
    process->start(cmd);
}
void Notepad::emailTextAsAttachment(){
    QString body = textEdit->toPlainText();
    QString outlookExe = "\"" + outlookPath + "OUTLOOK.EXE\"";
    QString cmd = outlookExe + outlookCmd;

    QProcess *process = new QProcess(this);
//    process->start(cmd);
    process->start("C:\Program Files (x86)\Microsoft Office\Office14\OUTLOOK.EXE");
}

void Notepad::closeEvent(QCloseEvent *event){
    qDebug() << "Window closed";

    if (maybeSave()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}
