#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QMainWindow>
#include <QJsonDocument>
#include <QProcess>

class QAction;
class QMenu;
class QPlainTextEdit;
class QNetworkReply;
class QWebView;
class QStackedWidget;

class QJsonDocument;

class QProcess;


class Notepad : public QMainWindow
{
    Q_OBJECT

public:
    Notepad();
    ~Notepad();

protected:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;

private slots: //Slots are functions that listen to events
//    void on_pushButton_clicked();

//    void on_actionOpen_triggered();

//    void on_actionSave_triggered();

//    void on_actionSave_As_triggered();
//    void write_to_file(QString fileName);
    void newFile();
    void open();
    bool save();
    bool saveAs();

    bool saveUserCredentialsForGoogleDrive();
    bool getAuthCodeForDrive();
    void getAuthToken();
    void getAuthTokenWithRefreshToken();

    void replyFinished(QNetworkReply *reply);
    void uploadFileFinihed(QNetworkReply *reply);

    void processAuthCode(QNetworkReply *reply);
    void processAuthToken(QNetworkReply *reply);
    void authPageUrlChanged();
    void authPageTitleChanged();
    void searchOutlookFinished(int code, QProcess::ExitStatus status);
    void searchOutlookError(QProcess::ProcessError error);

    void emailTextAsBody();
    void emailTextAsAttachment();
//    void about();
    void documentWasModified();
    bool uploadFile();
    bool getFiles();

private:
//    Ui::Notepad *ui;
    QString currentFileName;
    void createActions(); //Create action objects
    void createMenus(); //Create meny objects
    void createToolBars(); //Create tool bars
    void createStatusBar(); //Create status bar
    void readSettings();
    void writeSettings();
    void readTokens();
    bool maybeSave();
    void loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);
    void findOutlook();
    bool sendAsAttachmentInOutlook();

    QString strippedName(const QString &fullFileName);

    QString outlookPath, outlookCmd, redirectUri, clientId, clientSecret, refreshToken, accessToken, tokenType;

    bool disableDriveCredAct;

    QProcess *process;

    void initializeVariables();

    QWidget *main;
    QStackedWidget *stackedWidget;

    QPlainTextEdit *textEdit; //Predefined plantext ui object
    QString curFile;

    QString authCode;
    QString authToken;
    QJsonDocument authTokens;


    QWebView *webView;

    QMenu *fileMenu; //Predefined menu object
    QMenu *editMenu; //
    QMenu *saveCredMenu;
    QMenu *uploadCurrentFileMenu;
    QMenu *emailMenu;
    QMenu *helpMenu;
    QToolBar *fileToolBar;
    QToolBar *editToolBar;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *exitAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;

    QAction *saveCredAct;
    QAction *uploadCurrentFileAct;
    QAction *mailInOutlookAsBodyAct;
    QAction *mailInOutlookAsAttachmentAct;
//    QAction *aboutAct;
//    QAction *aboutQtAct;

    QString copyPng;

};

#endif // NOTEPAD_H
